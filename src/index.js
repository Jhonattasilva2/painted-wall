import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { PaintPanelContextProvider } from './contexts/PaintPanelContext';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <PaintPanelContextProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </PaintPanelContextProvider>
);


