import { createContext, useState } from "react";
import { useForm } from "react-hook-form";

export const PaintPanelContext = createContext();

export function PaintPanelContextProvider({ children }) {
  const { register, handleSubmit } = useForm();
  const [camposVazios, setCamposVazios] = useState(false);
  const [tamanhoParedes, setTamanhoParedes] = useState(false);
  const [resultado, setResultado] = useState(0);
  const tamanhoJanela = 2 * 1.2;
  const tamanhoPorta = 0.8 * 1.9;

  function validarCamposVazios(data) {
    if (
      isNaN(data.parede1) ||
      isNaN(data.parede2) ||
      isNaN(data.parede3) ||
      isNaN(data.parede4) ||
      isNaN(data.portaParede1) ||
      isNaN(data.portaParede2) ||
      isNaN(data.portaParede3) ||
      isNaN(data.portaParede4) ||
      isNaN(data.janelaParede1) ||
      isNaN(data.janelaParede2) ||
      isNaN(data.janelaParede3) ||
      isNaN(data.janelaParede4)
    ) {
      setCamposVazios(true);
    } else {
      setCamposVazios(false);
      litrosDeTinta(data);
      validarTamanhosPortaJanelaParede(data);
    }
  }

  function validarTamanhosPortaJanelaParede(data) {
    if (
      data.parede1 / 2 <= tamanhoJanela ||
      data.parede2 / 2 <= tamanhoJanela ||
      data.parede3 / 2 <= tamanhoJanela ||
      data.parede4 / 2 <= tamanhoJanela
    ) {
      setTamanhoParedes(true);
    } else {
      setTamanhoParedes(false);
    }
  }

  function litrosDeTinta(data) {
    const totalParede =
      data.parede1 + data.parede2 + data.parede3 + data.parede4;

    const totalPorta =
      (data.portaParede1 +
        data.portaParede2 +
        data.portaParede3 +
        data.portaParede4) *
      tamanhoPorta;

    const totalJanela =
      (data.janelaParede1 +
        data.janelaParede2 +
        data.janelaParede3 +
        data.janelaParede4) *
      tamanhoJanela;

    const litros = (totalParede - totalPorta - totalJanela) / 5;
    console.log(`${litros}L de tinta`);
    calculo(litros);
  }

  function calculo(litros) {
    const validators = [
      {
        condition: litros <= 0.5,
        message: "1 lata de 0,5",
      },
      {
        condition: litros > 0.5 && litros <= 1.0,
        message: "2 latas de 0,5",
      },
      {
        condition: litros > 1.0 && litros <= 1.5,
        message: "3 latas de 0,5",
      },
      {
        condition: litros > 1.5 && litros <= 2.5,
        message: "1 lata de 2,5",
      },
      {
        condition: litros > 2.5 && litros <= 3.0,
        message: "1 lata de 2,5L e uma de 0,5",
      },
      {
        condition: litros > 3.0 && litros <= 3.6,
        message: "1 lata de 3,6",
      },
      {
        condition: litros > 3.6 && litros <= 4.1,
        message: "1 lata de 3,6L e uma lata de 0,5",
      },
      {
        condition: litros > 4.1 && litros <= 4.6,
        message: "1 lata de 3,6L e 2 latas de 0,5",
      },
      {
        condition: litros > 4.6 && litros <= 5.1,
        message: "1 lata de 3,6L e 3 latas de 0,5",
      },
      {
        condition: litros > 5.1 && litros <= 5.6,
        message: "1 lata de 3,6L e 1 lata de 2,5",
      },
      {
        condition: litros > 6.1 && litros <= 6.6,
        message: "1 lata de 3,6L, 1 lata de 2,5 e uma lata de 0,5",
      },
      {
        condition: litros > 6.6 && litros <= 7.2,
        message: "2 latas de 3,6",
      },
      {
        condition: litros > 7.2 && litros <= 7.7,
        message: "2 latas de 3,6L e 1 lata de 0,5",
      },
      {
        condition: litros > 7.7 && litros <= 8.3,
        message: "2 latas de 3,6L e 2 latas de 0,5",
      },
      {
        condition: litros > 8.3 && litros <= 8.5,
        message: "2 latas de 3,6L e 3 latas de 0,5",
      },
      {
        condition: litros > 8.8 && litros <= 9.3,
        message: "2 latas de 3,6L e 1 lata de 2,5",
      },
      {
        condition: litros > 9.3 && litros <= 9.8,
        message: "2 latas de 3,6L e 1 lata de 2,5L e 1 lata de 0,5",
      },
      {
        condition: litros > 9.8 && litros <= 10.8,
        message: "3 latas de 3,6",
      },
      {
        condition: litros > 10.8 && litros <= 13.3,
        message: "3 latas de 3,6L e 1 de 2,5",
      },
      {
        condition: litros > 13.3 && litros <= 15.8,
        message: "4 latas de 3,6L e 1 de 2,5",
      },
      {
        condition: litros > 15.8 && litros <= 17,
        message: "4 latas de 3,6L e 2 de 2,5",
      },
      {
        condition: litros > 17 && litros <= 18,
        message: "1 lata de 18",
      },
      {
        condition: litros > 18 && litros <= 19,
        message: "1 lata de 18L e 2 latas de 0,5",
      },
      {
        condition: litros > 19 && litros <= 20.5,
        message: "1 lata de 18L e 1 lata de 2,5",
      },
      {
        condition: litros > 20.5 && litros <= 21.6,
        message: "1 lata de 18L e 1 lata de 3.6",
      },
      {
        condition: litros > 21.6 && litros <= 24.1,
        message: "1 lata de 18L e 1 lata de 3,6L e 1 lata de 2,5 ",
      },
      {
        condition: litros > 24.1 && litros <= 25.2,
        message: "1 lata de 18L e 2 latas de 3,6",
      },
      {
        condition: litros > 25.2 && litros <= 27.7,
        message: "1 lata de 18L e 2 latas de 3,6 e 1 lata de 2,5",
      },
      {
        condition: litros > 27.7 && litros <= 28.8,
        message: "1 lata de 18L e 3 latas de 3,6",
      },
      {
        condition: litros > 28.8 && litros <= 31.3,
        message: "1 lata de 18L e 3 latas de 3,6 e uma lata de 2,5",
      },
      {
        condition: litros > 31.3 && litros <= 32.4,
        message: "1 lata de 18L e 4 latas de 3,6",
      },
      {
        condition: litros > 32.4,
        message: "EM BREVE CALCULOS MAIORES",
      },
    ];

    for (const validator of validators) {
      if (validator.condition) {
        setResultado(validator.message);
        return;
      }
    }
  }

  return (
    <PaintPanelContext.Provider
      value={{
        register,
        handleSubmit,
        camposVazios,
        tamanhoParedes,
        resultado,
        validarCamposVazios,
      }}
    >
      {children}
    </PaintPanelContext.Provider>
  );
}
