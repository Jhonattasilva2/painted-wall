import React from 'react';
import styled from 'styled-components'
import FooterInformation from './components/FooterInformation';
import PaintPanel from './components/PaintPanel';


function App({ information }) {
  return (
    <Container>
      <Title>Vamos pintar as paredes?</Title>
      <PaintPanel />
      <FooterInformation information='* Cada porta possui 0.80 x 1.90m² e cada janela possui 2 x 1.20m²' />
      <FooterInformation information='* 1L de tinta é suficiente para pintar 5m²' />
    </Container>
  );
}

const Container = styled.div`
margin: 0;
padding: 0;
width: 100%;
background-color: #F5F5F5;
display: flex;
flex-direction: column;
`

const Title = styled.h1`
font-size: 2.5rem;
text-align: center;
`

export default App;
