import styled from "styled-components";
import { useContext } from "react";
import { PaintPanelContext } from "../contexts/PaintPanelContext";

export default function ResultPanel() {
  const { resultado } = useContext(PaintPanelContext);

  return (
    <InkPanel>
      <DescriptionResult>Você vai precisar de:</DescriptionResult>
      <Result theme={theme.green}>{resultado + "L"}</Result>
      <DescriptionResult>para pintar as paredes</DescriptionResult>
    </InkPanel>
  );
}

const InkPanel = styled.div`
  padding: 1rem;
  min-width: 300px;
  min-height: 80%;
  background-color: #fff;
  border-radius: 1rem;
  box-shadow: rgba(0, 0, 0, 0.5) 0px 5px 15px;
  margin: 2rem 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 2rem;
`;

const DescriptionResult = styled.h6``;

const Result = styled.p`
  color: ${(props) => props.theme};
  font-size: 1.5rem;
  font-weight: bold;
`;

const theme = {
  red: "#FF0000",
  blue: "#0000FF",
  yellow: "#FFFF00",
  green: "#008000",
};