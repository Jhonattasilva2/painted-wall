import React from 'react'
import styled from 'styled-components'

export default function FooterInformation({information}) {
  return (
    <Footer>{information}</Footer>
  )
}

const Footer = styled.footer`

text-align: center;;
padding: 0.5rem;
`
