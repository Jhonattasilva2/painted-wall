import React from 'react'
import styled from 'styled-components'

export default function SpanTamanhoParedes() {
  return (
    <Span>Parede pequena em relação ao tamanho da janela ou porta </Span>
  )
}

const Span = styled.span`
    font-size: 0.7rem;
    color: #FF0000;
`

