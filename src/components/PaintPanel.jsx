import { useContext } from "react";
import { PaintPanelContext } from "../contexts/PaintPanelContext";
import styled from "styled-components";
import SpanCamposVazios from "./SpanCamposVazios";
import SpanTamanhoParedes from "./SpanTamanhoParedes";
import ResultPanel from "./ResultPanel";
import InputPanel from "./InputPanel";

export default function PaintPanel() {
  const { handleSubmit, validarCamposVazios, camposVazios, tamanhoParedes } =
    useContext(PaintPanelContext);

  return (
    <TableContent>
      <ConfigPanel>
        <FormPanel onSubmit={handleSubmit(validarCamposVazios)}>
          <Label theme={theme.blue}>Quantos m² tem cada parede?</Label>
          <DivParedes>
            <InputPanel input={"parede1"} min={1} max={50} step={0.1} />
            <InputPanel input={"parede2"} min={1} max={50} step={0.1} />
            <InputPanel input={"parede3"} min={1} max={50} step={0.1} />
            <InputPanel input={"parede4"} min={1} max={50} step={0.1} />
          </DivParedes>

          <Label theme={theme.red}>Quantas portas tem cada parede?</Label>
          <DivParedes>
            <InputPanel input={"portaParede1"} min={0} max={3} />
            <InputPanel input={"portaParede2"} min={0} max={3} />
            <InputPanel input={"portaParede3"} min={0} max={3} />
            <InputPanel input={"portaParede4"} min={0} max={3} />
          </DivParedes>

          <Label theme={theme.green}>Quantas janelas tem cada parede?</Label>
          <DivParedes>
            <InputPanel input={"janelaParede1"} min={0} max={4} />
            <InputPanel input={"janelaParede2"} min={0} max={4} />
            <InputPanel input={"janelaParede3"} min={0} max={4} />
            <InputPanel input={"janelaParede4"} min={0} max={4} />
          </DivParedes>

          {camposVazios && <SpanCamposVazios />}
          {tamanhoParedes && <SpanTamanhoParedes />}

          <Button>Calcular</Button>
        </FormPanel>
      </ConfigPanel>
      <ResultPanel />
    </TableContent>
  );
}

const TableContent = styled.div`
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
  align-items: center;
`;

const ConfigPanel = styled.div`
  padding: 1rem;
  min-width: 300px;
  max-height: 80%;
  background-color: #fff;
  border-radius: 1rem;
  box-shadow: rgba(0, 0, 0, 0.5) 0px 5px 15px;
  margin: 2rem 0;
`;

const FormPanel = styled.form`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Label = styled.span`
  color: ${(props) => props.theme};
  display: block;
  margin-bottom: 1rem;
  font-size: 1.2rem;
  font-weight: bold;
  transition: all 0.4s;
`;

const theme = {
  red: "#FF0000",
  blue: "#0000FF",
  yellow: "#FFFF00",
  green: "#008000",
};

const DivParedes = styled.div`
  width: 100%;
  height: 3.5rem;
  display: flex;
  justify-content: space-around;
  margin-bottom: 2rem;
`;

const Button = styled.button`
  width: 70%;
  height: 2rem;
  border: none;
  border-radius: 0.3rem;
  font-size: 0.8rem;
  background-color: #4169e1;
  color: #fff;
  margin-top: 0.9rem;
  cursor: pointer;
  transition: all 0.4s;

  &:hover {
    background-color: #0000ff;
  }
`;
