import React from "react";
import styled from "styled-components";
import { useContext } from "react";
import { PaintPanelContext } from "../contexts/PaintPanelContext";

export default function InputPanel({ input, min, max, step }) {
  const { register } = useContext(PaintPanelContext);
  return (
    <DivParede>
      <SpanPerede>Parede 1</SpanPerede>
      <InputParede
        type="number"
        {...register(`${input}`, { valueAsNumber: true })}
        min={min}
        max={max}
        step={step}
        placeholder="0"
      />
    </DivParede>
  );
}

const DivParede = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const SpanPerede = styled.span`
  margin-bottom: 0.3rem;
`;

const InputParede = styled.input`
  width: 2.9rem;
  height: 2rem;
  font-size: 1rem;
  border-radius: 0.3rem;
  border: 1px solid #000;
`;
