import React from 'react'
import styled from 'styled-components'

export default function SpanCamposVazios() {
  return (
    <Span>Preencha todos os campos</Span>
  )
}

const Span = styled.span`
    font-size: 0.8rem;
    color: #FF0000;
`
