# PAINTED WALL

Para poder fazer rodar o webapp em sua máquina você primeiro precisa ter o pacote NPM instalado, com ele 
instalado você precisa no diretório que você clonou ter a pasta Node modules, para isso abra o cmd no seu diretório e digite o código `npm install` após isso você estará apto para seguir o script abaixo.

## Available Script

### `npm start`

Execute o aplicativo no modo de desenvolvedor.\
Abra [http://localhost:3000](http://localhost:3000) para visualizar em seu browser.

A página irá recarregar sempre que você fizer alguma mudança no código.\


## Tabela de conteúdos

- [Overview](#overview)
  - [O Desafio](#oDesafio)
  - [Screenshot](#screenshot)
  - [Links](#links)
  - [Criado com](#built-with)
  - [O que eu aprendi](#what-i-learned)
  - [pretensão de desenvolvimento](#continued-development)
- [Autor](#author)
- [Agradecimentos](#acknowledgments)



## Overview
   Esse aplicativo web construído com React.js e Styled Components tem como objetivo mostrar qual a quantidade correta de latas de tinta que é necessário comprar parar pintar 4 paredes, dependendo do seu tamanho, para que o usuário não compre na quantidade errada. O aplicativo leva em conta o tamanho em m² das paredes, portas (com tamanho padrão) e janelas (também com tamanho padrão) para enfim calcular. 
   Mesmo sendo na lógica relativamente fácil criar esse aplicativo ele foi construído com uma tecnologia mais avançada e diferente do que seria necessário para criá-la à pedido da empresa.

### O desafio

Usuários devem ser capazes de:

- Colocar a metragem das quatro paredes
- Colocar quantas portas possuui cada parede
- Quantas janelas possui cada parede
- Calcular quantas latas de tinta e o tamanho são necessárias para pintar as paredes especificada nos inputs.
- Ver o resultado no card menor ao lado

### Screenshot

![Screenshot](./src/img/paintedwall.png)



### Links

- Solution URL: [https://painted-wall.vercel.app/](https://painted-wall.vercel.app/)

### Criado com

- [React](https://reactjs.org/) - JS library
- [React Hook Form](https://react-hook-form.com/) - React library para formulários
- [Styled Components](https://styled-components.com/) - For styles
- [Context API](https://pt-br.reactjs.org/docs/context.html) - Hook do React



### O que eu aprendi?

Esse projeto sem dúvidas foi um divisor de água para mim, meu primeiro projeto usando o 
Context API, certamente não será o último mas com certeza um grande avanço desde então.


### Pretensões de desenvolvimento

Pretendo ainda implementar uma função para adicionar mais input de portas, janelas e paredes; também
pretendo tornar o projeto mais e mais escalável com o tempo permitindo calcular até mesmo o necessários para se pintar um prédio.


## Autor

- LinkedIn - [Jhonatta Silva](https://www.linkedin.com/in/jhonatta-silva-dev/)
- GitHub - [Jhonatta Silva](https://github.com/Jhonattasilva2)
- Twitter - [@yourusername](https://www.twitter.com/yourusername) 

## Agradecimentos

Gostaria de agradecer a empresa Digital Republic pela oportunidade de fazer parte do processo seletivo me
propondo este Code Challenge, confesso que aprendi muito com ele e espero poder atuar como Desenvolvedor Frontend júnior com vocês!


